<?php
date_default_timezone_set("America/New_York"); 
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
    // Enable use of the session
   session_start();
    
    // Require files
   
   require('models/connect.php');
   require('models/user.php');
   require('models/usermapper.php');
    
    // Check to see if the user is logged in,
    // Else die so the page doesn't load.
  if ( !isset($_SESSION['is_logged_in'])) {
       header("Location: login.php");
    }
    
    
    /*
     * This is used to retain form data for the radio posts
     * if there are errors when the form is submitted.
     * @param string $itemValue
     * @param array $itemName
     * @param string $itemValue
     * @return string $output
     * 
     */
    function isChecked($itemName, $itemValue, $output = 'checked')
{
    if (isset($_POST[$itemName]) && $_POST[$itemName] == $itemValue) {
        return $output;
    } 
    else {
        return '';
    }
}
    
?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
   
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css">
    <link type="text/css" rel="stylesheet" href="css/footer.css">
    <link type="text/css" rel="stylesheet" href="css/map.css">
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

    <title>Rocket Televisions | New Product</title>
    <!-- Favicon -->
    <link type="image/ico" rel="shortcut icon" href="favicon.ico">

</head>
<body>
    <nav class="navbar navbar-default navbar-static-top">
     
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
            <a class="navbar-brand" href="#"><img src="images/logo_main_small.png" alt="Logo" style="margin-top:-12px"> Rocket Televisions</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Home</a></li>
            <li><a href="about.php">About Us</a></li>
            <li><a href="secure.php">Dashboard</a></li>
           <li class="active"><a href="add.php">Add New Product</a></li>
           <li><a href="logout.php">Log Out</a></li>
         </ul>
            <ul class="nav navbar-nav-right">
                <li class="pull-right"><h4>Welcome <?php echo $_SESSION['username'];?>!</h4></li>
          </ul>
        </div><!--/.nav-collapse -->
      
    </nav>
<div class="container">
    
    <h2 class="text-center">Add New Television</h2>
    <hr>
    <?php
    
    /* When a form is submitted, it will be validated for errors
     * prior to the form to be submitted into the database. 
     * @param string = $v 
     * @param array = $error
     */
        require('vendor/autoload.php');
        use SimpleValidator\Validator;
        use SimpleValidator\Validators;
    
    if (count($_POST) > 0)  
    {
$v = new Validator($_POST, array(
    new Validators\Required ('brand','Brand is required'),
    new Validators\AlphaNumeric('brand', 'A brand must be Alphanumeric'),
    new Validators\Required('size','Size of a television is required'),
    new Validators\Numeric('size', 'Size must be numeric'),
    new Validators\Required('price','Price of a television is required'),
    new Validators\Numeric('price', 'A Price must be numeric')
));

        // Validation error
if (!$v->execute()) 
{	
	echo "<p> You did not fill out all the required values. </p>";
    foreach ($v->getErrors() as $error)
    {
        echo "<p>" . $error[0] . "</p>" ;
    }

}

        
    }
    
    /*
     * This gets the form data ready for submission.
     * The $_POST values are shortened to make it easier
     * to insert data
     * @param string = $brand
     * @param string = $model
     * @param interger = $size
     * @param string = $res
     * @param string = $type
     * @param interger = $price
     * @param string = $notes
     * 
     */
    
    if (isset($_POST['submit']))
    {
        $brand = $_POST['brand'];
        $model = $_POST['model'];
        $size = $_POST['size'];
        $res =  $_POST['resolution'];
        $type = $_POST['type']; 
        $price = $_POST['price'];
        $notes = $_POST['notes'];
        
        // Insert product files into the database
        require('library/pdo.php');
        require('library/products.php');  
        $pm = new ProductMapper();
       $result3 = $pm->addProduct($brand, $model, $size, $res, $type, $price, $notes); 
    }     
?>
    <form class="form-horizontal" name="product" id="product" method="post"> 
        <fieldset>
		<!-- Brand Maker-->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="brand">Brand</label>  
		  <div class="col-md-3">
                  <input type="text" name="brand" id="brand" class="form-control"  value="<?php if (count($_POST['brand'] > 0)){ echo $_POST['brand']; };?>" placeholder="Brand of Television" />
		  </div>
		  </div>
	
		<!-- Model-->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="model">Model Number: *</label>  
                    <div class="col-md-3">	
                        <input type="text" name="model" class="form-control" id="model" placeholder="Model Number" value="<?php if (count($_POST > 0)){ echo $_POST['model']; }?>" />
                    </div>
		</div>

		<!-- Size-->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="size">Size of Television *</label>  
                        <div class="col-md-1">
                            <input type="text" name="size" id="size" class="form-control" value="<?php if (count($_POST > 0)){ echo $_POST['size']; }?>" placeholder="32" />
			</div>
		  </div>
                
		<!-- Resolution -->
		<div class="form-group">
		  <label class="col-md-4 control-label">Resolution:</label>
                   <label class="radio-inline">
                       <input type="radio" name="resolution" value="480p" <?php echo isChecked('resolution', '480p') ?>/> 480p
                   </label>
                   <label class="radio-inline">
                        <input type="radio" name="resolution" value="720p" <?php echo isChecked('resolution', '720p') ?>/> 720p
                   </label>
                    <label class="radio-inline">
                        <input type="radio" name="resolution" value="1080i" <?php echo isChecked('resolution', '1080i') ?>/> 1080i
                    </label>                  
                    <label class="radio-inline">
                        <input type="radio" name="resolution"  value="1080p" <?php echo isChecked('resolution', '1080p') ?>> 1080p
                    </label>
                    </div>
	
                <!-- TV Type-->
                <div class="form-group">
		  <label class="col-md-4 control-label">Type:</label>
                   <label class="radio-inline">
                        <input type="radio" name="type" value="CRT" <?php echo isChecked('type', 'CRT') ?>/> CRT
                   </label>
                   <label class="radio-inline">
                        <input type="radio" name="type" value="LED" <?php echo isChecked('type', 'LED') ?>/> LED
                   </label>
                    <label class="radio-inline">
                        <input type="radio" name="type" value="Plasma" <?php echo isChecked('type', 'Plasma') ?>/> Plasma
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="type" value="3D" <?php echo isChecked('type', '3D') ?>/> 3D
                    </label>                  
                    </div>
                
                		<!-- Price-->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="price">Price *</label>  
		  <div class="col-md-2">
				<input type="text" name="price" id="price" class="form-control" value="<?php if (count($_POST > 0)){ echo $_POST['price']; }?>" placeholder="100.00" />
			</div>
		  </div>
                
		<!-- Notes -->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="notes">Product Description:</label>
		  <div class="col-md-4">               
			<textarea name="notes" id="notes" class="form-control"></textarea>
		  </div>
		</div>

		<!-- Submit Button -->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="submit"></label>
		  <div class="col-md-4">
                      <button type="submit" name="submit" id="submit" class="btn btn-primary btn-lg"> Submit</button>
			<button type="reset" class="btn btn-default btn-lg" value="Reset" title="Reset to type the information again">	<i class="fa fa-times"></i> Reset </button>  
		  </div>
		</div>
	</fieldset>
                </form>
        </div>
</body>   
		</html>
                   