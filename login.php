<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 2/20/2015
 * Time: 6:07 PM
 * 
 * 
 */
/*date_default_timezone_set("America/New_York"); 
error_reporting(E_ALL);
ini_set('display_errors', 1);
*/
/*
 * These files calls out the 
 * function that will get the 
 * products from the database
 * and the connection that it 
 * has to use to do just that.
 * 
 * 
 * 
 */

    // Enable use of the session
    session_start();
    
    // Require files
    require('models/connect.php');
    require('models/user.php');
    require('models/usermapper.php');

    $userMapper = new UserMapper();

    // If form was submitted, then process it.
    if (count($_POST) > 0) {

        $un = $_POST['username'];
        $pw = $_POST['password'];
        
        try {
            $userObject = $userMapper->getUser($un);                    

            // Check to see if the user is in the database
            if ($pw == $userObject->getPassword()) {
                
                $_SESSION['is_logged_in'] = true;
                $_SESSION['username'] = $un;        
                header("Location: add.php");
            }
            else {
                $msg = "Your user credentials are Invalid.  Please try entering it in again.";
            } 
            
        } catch (Exception $ex) {
             $msg = "Your user credentials are Invalid. Please try entering it in again.";
        }     
    }
?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Login | Rocket Televisions</title>
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css">
    <link type="text/css" rel="stylesheet" href="css/login.css">
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link type="image/ico" rel="shortcut icon" href="favicon.ico">
</head>

<body> 
      <nav class="navbar navbar-default navbar-static-top">
      
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
            <a class="navbar-brand" href="#"><img src="images/logo_main_small.png" alt="Logo" style="margin-top:-12px"> Rocket Televisions</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
           
            <li><a href="index.php">Home</a></li>
            <li><a href="about.php">About Us</a></li> 
            <li class="active"><a href="login.php">Login</a></li>
			
          </ul>
        </div><!--/.nav-collapse -->

    </nav>
    
<div class="container">
   
	
	<div class="col-md-7 col-md-offset-2">
            <p>Login user: nancy <br /> Password: nancy</p> <br />
      <form class="form-signin"  method="post">
          <?php if (isset($msg)) { echo '<div class="alert alert-danger" role="alert">'  . $msg . '</div>'; } ?> 
        <h2 class="form-signin-heading">Enter your user credentials to sign in </h2>
		<div class ="form-group">
        <label for="inputEmail" class="sr-only">Username</label>
        <input type="text" name="username" id="username" class="form-control" value="<?php if (count($_POST > 0)){ echo $_POST['username']; }?>" placeholder="Username" required autofocus>
        </div>
		<div class ="form-group">
		<label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="password" value="<?php if (count($_POST > 0)){ echo $_POST['password']; }?>" class="form-control" placeholder="Password" required>
        </div>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->
    </div> <!-- /container -->
      
  </body>
</html>