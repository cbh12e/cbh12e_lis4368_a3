<?php

session_start();
 require('models/connect.php');
    require('models/user.php');
    require('models/usermapper.php');
    
    if ( ! isset($_SESSION['is_logged_in'])) {
        header("Location: login.php");
    }
    else
    {
        session_destroy();
        header("Location: index.php");
    }
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

