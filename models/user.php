<?php


/**
 * Description of User
 *
 * @author casey
 */
class User 
{
    protected $id;
    
    protected $username;
    
    protected $password;

    /**
     * Constructor
     * 
     * @param string   $username
     * @param string   $password
     * @param int|null $id
     */
    public function __construct($username, $password, $id = null)
    {
        $this->username = $username;
        $this->password = $password;
        $this->id = $id;
    }
    
    public function getId() {
        return $this->id;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getPassword() {
        return $this->password;
    }


}

