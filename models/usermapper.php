<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserMapper
 *
 * @author casey
 */
class UserMapper 
{
    /**
     * Get all users
     */
    public function getUsers()
    {
        $db = getDbConnection();
        $stmt = $db->prepare("SELECT * FROM users");
        $stmt->execute();
        
        $out = array();
        
        while ($row = $stmt->fetch()) {
            $out[] = new User($row['username'], $row['password'], $row['id']);
        }
        
        return $out;
    }
    
    /**
     * Get a single user by their username
     * 
     * @param string $username
     */
    public function getUser($username)
    {
        $db = getDbConnection();
        $stmt = $db->prepare("SELECT * FROM users WHERE username = ?");
        $stmt->execute(array($username));

        // Get one row from query result / will return FALSE if no row
        $row = $stmt->fetch();
        
        if ($row !== false) {
            return new User($row['username'], $row['password'], $row['id']);
        }
        else {
            throw new Exception("Could not find username: " . $username);
        }
        
    }   
}