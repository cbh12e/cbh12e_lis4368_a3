<?php

/**
 * @package
 * Get a database connection 
 * from MySQL using PDO drivers.
 * $Class SQL
 * @abstract SQL  
 * @access protected           The Properties used are protected
 * @param string $host         Hostname used to connect to MySQL
 * @param string $db_name      Database used to connect to MySQL
 * @param string $username     Username used to connect to MySQL
 * @param string $password     Password used to connect to MySQL
 * @return array PDO           Return the results of query from MySQL
 
 */
class SQL
{
        protected $host;
        protected $db_name;
        protected $username;
        protected $password;

public function getConnection()
{       
        $host= "localhost";
        $db_name = "cbh12e_lis4368_a3";
        $username = "root";
        $password = "password";
       
        return new PDO("mysql:host={$host};dbname={$db_name}", $username, $password);
}

}