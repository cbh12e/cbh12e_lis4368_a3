<?php
/**
 * @package 
 *  ProductMapper is the class of this database.
 *  There are two functions getProducts and getProduct
 */
class ProductMapper
{
    /* This will return the results of all of the products listed
     * in the database.
     * @access protected
     * @param string $a     Calling the SQL class to get a connection.*
     * @param string $c     Used to call the getProducts class
     * @return getConnection  Returns all results into a row
     */

    protected $stmt;
    protected $a;

    public function getProducts()
    {
        $a = new SQL();
        $c = $a->getConnection();


        $stmt = $c->prepare("SELECT * from product");
        $stmt->execute();

        // The results will now be fetched
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    // Return an array of `Product` objects

    /*
     * @access protected
     * @param int = $id    Calling the number for the single product to call
     * @param string = $stmt    Use to get the results for the SQL Statement
     * @return array $results         Gets the results from the row before being fetched
     */

    protected $results;
    protected $id;

    public function getProduct($id)
    {   /*
         * This will get the single frow from
         * the database to convert it into a
         * product object.
         *        
        /* @param string = $a       Calling the SQL Class to set up a new connection
         * @param string = $db       Connection to recieve the product from the getProduct function
         * @return array = $results The results of the product will now be fetched.
         */

        $a = new SQL();
        $db = $a->getConnection();

        $stmt = $db->prepare("SELECT * from product where pro_id = ?");

        $stmt->execute(array($id));

        // The results will now be fetched
        $results = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($results == true) {
            return $results;
        } else 
            {
            throw new Exception ("Product not found " . $id);
        }
    }
            
    protected $brand;
    protected $model;
    protected $size;
    protected $res;
    protected $type;
    protected $price;
    protected $notes;


        // 

    /*
     * This is used to insert products into 
     * the database. The form data that is used comes from
     * add.php.
     * 
     * @access protected
     * @param string = $brand    Calling the number for the single product to call
     * @param string = $model
     * @param interger = $size
     * @param string = $res
     * @param string = $type
     * @param interger = $price
     * @param array = $result3        Gets the results from the row before being fetched
     */

    
    public function addProduct($brand, $model, $size, $res, $type, $price, $notes)
    { 
        $am = new SQL();
        $ad = $am->getConnection();
        $pm = $ad->prepare("INSERT into product (pro_name, pro_model, pro_size, pro_resolution, pro_type, pro_price, pro_description)
                            VALUES( :brand , :model, :size, :resolution, :type, :price, :notes)");
        $result3 = $pm->execute(array(':brand' => $brand, ':model' => $model, ':size' => $size, ':resolution' => $res, ':type' => $type, ':price' => $price , ':notes' => $notes ));        

        if($result3 == true)
       {
           echo "True";
           header("Location: index.php");
       }
       
       else
       {
           echo "False";
       }
    }

}