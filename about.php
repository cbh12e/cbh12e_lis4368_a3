<?php  
    session_start();
  // Require files
    require('models/connect.php');
    require('models/user.php');
    require('models/usermapper.php');
?>    

<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Rocket Televisions | About Us</title>
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css">
    <link type="text/css" rel="stylesheet" href="css/footer.css">
    <link type="text/css" rel="stylesheet" href="css/map.css">
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <!-- Favicon -->
    <link type="image/ico" rel="shortcut icon" href="favicon.ico">

</head>
<body>
    <nav class="navbar navbar-default navbar-static-top">
     
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
            <a class="navbar-brand" href="#"><img src="images/logo_main_small.png" alt="Logo" style="margin-top:-12px"> Rocket Televisions</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
               <?php   
           if ( !isset($_SESSION['is_logged_in'])) {  
                 echo'<li><a href="index.php">Home</a></li>';
                 echo'<li class="active"><a href="about.php">About Us</a></li>';
                 echo'<li><a href="login.php">Login</a></li>';
                 echo'</ul>';
                    echo'<ul class="nav navbar-nav-right">';
                 echo'<li class="pull-right"><h3>You are not logged in!</h3></li>';
            }
             else
             {
                    echo'<li><a href="index.php">Home</a></li>';
                    echo'<li class="active"><a href="about.php">About Us</a></li>';
                    echo'<li ><a href="secure.php">Dashboard</a></li>';
                   echo'<li><a href="add.php">Add New Product</a></li>';
                   echo' <li><a href="logout.php">Log Out</a></li>';
                   echo'</ul>';
                    echo'<ul class="nav navbar-nav-right">';
                    echo'<li class="pull-right"><h4>Welcome '. $_SESSION['username'] .'!</h4></li>';
             }
                    ?>
          </ul>
        </div><!--/.nav-collapse -->
      
    </nav>
    
<div class ="container">

    <img class="img-responsive pull-right" src="images/logo_main.png" alt="Logo" id="teamRocket">
    <h1 class="text-center" style="font-size:55px;">About Rocket Televisions</h1> 
    <hr>
 
    <div class="col-md-7"> 
        <p style="font-size:17px;">
        Since the founding of Rocket Televisions in 2015, Rocket Televisions is a family owned and operated business that specializes in selling televisions to customers nationwide. Rocket Televisions only sells top of the line 
        televisions that will satisfy you or we will refund you if you are not satisified with our products within a thirty day period. Rocket Television's roots come from Monticello, a rural town in the Florida Panhandle located 30 miles east of the state capital: Tallahassee, Florida.
        
        Since Rocket Televisions specializes in television sets, we sell the major brands of televisions which includes Cosmo, Samsung, Panasonic, LG, Sony, Sharp, Zenith and more. What is better about us is that we unlike the big box retailers is that we sell our televisions for less. The best about us is that we will ship our television sets right to your door anywhere in the United States from our brand new efficient 100,000 square foot warehouse powered by 100% of renewable energy from Lake Talquin.</p>
    
    </div>
    
  <!-- Google Maps Location iFrame-->
  <div class="col-md-5">
<iframe class="img-responsive" id="map" src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d220014.978363252!2d-84.01447006328124!3d30.50027651293717!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sus!4v1425184649329"></iframe>        
   </div>
</div>    
<?php 

/*
 * This calls out the footer page
 * for the document, to allow the
 * footer page and JavaScript 
 * files to be called out
 * to be edited once and not 
 * to be done for every single page.
 */
require_once("footer.php");  // Footer and JavaScript Scripts

?>
</body>
</html>