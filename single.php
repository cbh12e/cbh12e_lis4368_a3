<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 2/20/2015
 * Time: 6:07 PM * 
 * 
 */

 session_start();
  // Require files
    require('models/connect.php');
    require('models/user.php');
    require('models/usermapper.php');
    
    
date_default_timezone_set("America/New_York"); 
error_reporting(E_ALL);
ini_set('display_errors', 1);

require('library/pdo.php');
require('library/products.php');

/* 
 * This calls out the productMapper to get the item
 * @string = $id 
 * @param = $pm
 * 
 * 
 */

$id = 8;
$pm = new ProductMapper();
$row = $pm->getProduct($id);

?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Products</title>
    <!-- Cascade Style Sheets for webpage -->
     <link rel="stylesheet" href="css/bootstrap.css">
     <link rel="stylesheet" href="css/footer.css">
     <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
     <link rel="shortcut icon" type="image/ico" href="favicon.ico" />
</head>
<body>
    
      <nav class="navbar navbar-default navbar-static-top">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
            <a class="navbar-brand" href="#"><img src="images/logo_main_small.png" alt="Logo" style="margin-top:-12px"> Rocket Televisions</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
             <?php   
           if ( !isset($_SESSION['is_logged_in'])) {  
                 echo'<li class="active"><a href="index.php">Home</a></li>';
                 echo'<li><a href="about.php">About Us</a></li>';
                 echo'<li><a href="login.php">Login</a></li>';
            }
             else
             {
                    echo'<li class="active"><a href="index.php">Home</a></li>';
                    echo'<li><a href="about.php">About Us</a></li>';
                    echo'<li><a href="secure.php">Dashboard</a></li>';
                    echo'<li><a href="add.php">Add New Product</a></li>';
                    echo'<li><a href="logout.php">Log Out</a></li>';
             }
                    ?>
          </ul>
        </div><!--/.nav-collapse -->
    </nav>
    
   
    
<div class ="container">
    
    <h1 class="text-center"> Our Featured Product</h1>
     <button class="btn btn-primary" onclick="goBack()">See all Televisions</button>
     
     
     
    <table class="table table-striped">
        <thead>
        <tr>
            <td> Product ID</td>
            <td> Maker</td>
            <td> Model </td>
            <td> Size</td>
            <td> Resolution </td>
            <td> Type </td>
            <td> Price</td>
            <td> Description</td>
        </tr>
        <tbody>
    <?php 
{ 
    echo "<tr>";
    echo "<td>" .$row['pro_id']. "</td>";
    echo "<td>" .$row['pro_name']. "</td>";
    echo "<td>" .$row['pro_model']. "</td>";
    echo "<td>" .$row['pro_size']. "</td>";
    echo "<td>" .$row['pro_resolution']. "</td>";
    echo "<td>" .$row['pro_type']. "</td>";
    echo "<td> $" .$row['pro_price']. "</td>";
    echo "<td>" .$row['pro_description']. "</td>";
    echo "</tr>";
}
?>     
        </tbody> 
    </table>
</div>
<?php 

/*
 * This calls out the footer page
 * for the document, to allow the
 * footer page and JavaScript 
 * files to be called out
 *  to be edited once and not 
 * to be done for every single page.
 */
    require_once("footer.php");  // Footer and JavaScript Scripts

    
    
?>
    
<script type="text/javascript">
function goBack() {
    window.history.back();
}
</script>
</body>
</html>