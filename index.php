<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 2/20/2015
 * Time: 6:07 PM
 * 
 * 
 */
   session_start();
  // Require files
    require('models/connect.php');
    require('models/user.php');
    require('models/usermapper.php');
    
    
// Check to see if the user is logged in,
    // Else die so the page doesn't load.


date_default_timezone_set("America/New_York"); 
error_reporting(E_ALL);
ini_set('display_errors', 1);

    

/*
 * These files calls out the 
 * function that will get the 
 * products from the database
 * and the connection that it 
 * has to use to do just that.
 * 
 * 
 */
    require('library/pdo.php');
    require('library/products.php');
    
   /*
    * @param  string $pm Calls out the new class
    * @param  string $ab Calls out the Get Products class
    * @param  string $row Returns the results for the MySQL Connection
    * @param string $exception Gets the error message if there is an error
    * @return $msg This will print out the message if there is an error.
    */
try{
    $pm = new ProductMapper();
    $ab = $pm->getProducts();
}
catch (PDOException $exception) // Catch the error
	{
                $msg = "exception:". $exception ; // Print error
		return $msg->getMessage(); // Get error message    
        }
?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Products</title>
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css">
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link type="image/ico" rel="shortcut icon" href="favicon.ico">
</head>

<body> 
      <nav class="navbar navbar-default navbar-static-top">
      
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
            <a class="navbar-brand" href="#"><img src="images/logo_main_small.png" alt="Logo" style="margin-top:-12px"> Rocket Televisions</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
           <?php   
           if ( !isset($_SESSION['is_logged_in'])) {  
                 echo'<li class="active"><a href="index.php">Home</a></li>';
                 echo'<li><a href="about.php">About Us</a></li>';
                 echo'<li><a href="login.php">Login</a></li>';
                 echo"</ul>";
                 echo'<ul class="nav navbar-nav-right">';
                    echo'<li class="pull-right"><h3>You are not logged in!</h3></li>';
            }
             else
             {
                    echo'<li class="active"><a href="index.php">Home</a></li>';
                    echo'<li><a href="about.php">About Us</a></li>';
                    echo'<li><a href="secure.php">Dashboard</a></li>';
                    echo'<li><a href="add.php">Add New Product</a></li>';
                    echo'<li><a href="logout.php">Log Out</a></li>';
                    echo'</ul>';
                    echo'<ul class="nav navbar-nav-right">';
                    echo'<li class="pull-right"><h4>Welcome '. $_SESSION['username'] .'!</h4></li>';
          
             }
                    ?>
          </ul>
        </div><!--/.nav-collapse -->

    </nav>
    
<div class="container">
   
    <h1 class="text-center">Rocket Television's Best Televisions currently in stock!</h1>
      <a href="single.php" class="btn btn-primary" role="button">Our Featured Product</a>
    <table class="table table-striped" id="tv">
        <thead>
        <tr>
            <td> Product ID</td>
            <td> Maker</td>
            <td> Model</td>
            <td> Size </td>
            <td> Resolution </td>
            <td> Type </td>
            <td> Price</td>
            <td> Description</td>
        </tr>
      
        <tbody>
    <?php
foreach ($ab as $row)
{ 
    echo "<tr>";
    echo "<td>" .$row['pro_id']. "</td>";
    echo "<td>" .$row['pro_name']. "</td>";
    echo "<td>" .$row['pro_model']. "</td>";
    echo "<td>" .$row['pro_size']. "</td>";
    echo "<td>" .$row['pro_resolution']. "</td>";
    echo "<td>" .$row['pro_type']. "</td>";
    echo "<td> $" .$row['pro_price']. "</td>";
    echo "<td>" .$row['pro_description']. "</td>";
    echo "</tr>";
}


?>
            
        </tbody> 

     
    </table>
      
      </div>
<?php 

/*
 * This calls out the footer page
 * for the document, to allow the
 * footer page and JavaScript 
 * files to be called out
 *  to be edited once and not 
 * to be done for every single page.
 */
require_once("footer.php");  // Footer and JavaScript Scripts

?>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js"></script>
    <script type="text/javascript">
$(document).ready(function() {
    $.extend( $.fn.dataTable.defaults, {
    searching: false,
    ordering:  false
} );
    $('#tv').dataTable( {
		"orderMulti": false,
		"bSort" : false,
        "pagingType": "full_numbers",
		"lengthMenu": [ 5, 10, 20,30 ],
		"pageLength": 10
    } );
} );
    </script>
  </body>
</html>