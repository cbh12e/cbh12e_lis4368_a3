<?php

    // Enable use of the session
    session_set_cookie_params('3600'); 
    session_start();
    
    // Require files to see if user is logged in
    require('models/connect.php');
    require('models/user.php');
    require('models/usermapper.php');
    
    // Check to see if the user is logged in,
    // Else die so the page doesn't load.
    if ( ! isset($_SESSION['is_logged_in'])) {
        header("Location: login.php");
    }
    
    // Call userMapper class to get a list of all users
    $userMapper = new UserMapper();
    $userList = $userMapper->getUsers();
    
?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Rocket Televisions | About Us</title>
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css">
    <link type="text/css" rel="stylesheet" href="css/footer.css">
    <link type="text/css" rel="stylesheet" href="css/map.css">
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <!-- Favicon -->
    <link type="image/ico" rel="shortcut icon" href="favicon.ico">

</head>
<body>
    <nav class="navbar navbar-default navbar-static-top">
     
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
            <a class="navbar-brand" href="#"><img src="images/logo_main_small.png" alt="Logo" style="margin-top:-12px"> Rocket Televisions</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Home</a></li>
            <li><a href="about.php">About Us</a></li>
            <li class="active"><a href="secure.php">Dashboard</a></li>
           <li><a href="add.php">Add New Product</a></li>
            <li><a href="logout.php">Log Out</a></li>
          </ul>
          <ul class="nav navbar-nav-right">
                <li class="pull-right"><h4>Welcome <?php echo $_SESSION['username'];?>!</h4></li>
          </ul>
        </div><!--/.nav-collapse -->
      
    </nav>
    
<div class ="container">

    <img class="img-responsive pull-right" src="images/logo_main.png" alt="Logo" id="teamRocket">
    <h1 class="text-center" style="font-size:55px;">Dashboard</h1> 
    <hr>
 
        <ol>
        <?php
        
        /*
         * @param $userList
         * @param $user
         */
        foreach ($userList as $user) {
            echo "<li>" . $user->getUsername() . "</li>";
        }
        
        ?>
        </ol>
</div>    
<?php 

/*
 * This calls out the footer page
 * for the document, to allow the
 * footer page and JavaScript 
 * files to be called out
 * to be edited once and not 
 * to be done for every single page.
 */
require_once("footer.php");  // Footer and JavaScript Scripts

?>
        

    </body>
</html>