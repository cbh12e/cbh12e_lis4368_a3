<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of newProduct
 *
 * @author hargcb
 */


class newProduct
  {
    
         protected $brand;
         protected $model;
         protected $size;
         protected $res;
         protected $type;
         protected $price;
         protected $notes;
         
         public function __construct($brand, $model, $size, $res, $type, $price, $notes)       
         {
             $this->getBrand = $brand;
             $this->getModel = $model;
             $this->getSize = $size;
             $this->getRes = $res;
             $this->getType = $type;
             $this->getPrice = $price;
             $this->getNotes = $notes;
         }
         
      public function getBrand()
     {
       return $this->getBrand;
     }
     public function getModel()
     {
      return  $this->getModel;
     }
   
      public function getSize()
     {
      return $this->getSize;
     }
      public function getRes()
     {
      return $this->getRes;
     }
      public function getType()
     { 
      return $this->getType;
     }
     public function getPrice()
     {
      return $this->getPrice;
     }
     public function getNotes()
     {
       return $this->getNotes;
     }          
}  
